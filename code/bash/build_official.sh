#!/bin/bash

# Usage: build_official.sh [update]
# Script to build the essential fifa_official database (download data from
# the repositories and build the football.db).
# It also updates the fatabase with the latest matches information from
# the repositories.

# For more information about the database directory structure please visit:
# https://github.com/openfootball/build

opwd=$(pwd)
openfootballPath="../../dataBases/fifa_official/openfootball/"
if [ "$1" == "update" ]; then
	cd $openfootballPath/world-cup/
	git pull origin master
	cd ../build/
	rake update DATA=worldcup2014
	cd $opwd/../java/quiniela/
	# Update quinielas ranking database and translate official database
	./update_quinielas_rankingDB.sh
	cd $opwd
	./translateDB.sh ES $openfootballPath/build/build/football.db
	
else
	officialPath="../../dataBases/fifa_official"
	mkdir $officialPath
	mkdir openmundi
	cd openmundi
	git clone https://github.com/openmundi/world.db.git
	cd ../
	mkdir openfootball
	git clone https://github.com/openfootball/world-cup.git	
	git clone https://github.com/openfootball/build.git
	git clone https://github.com/openfootball/national-teams.git
	cd $opwd
	./build_official.sh update
fi
