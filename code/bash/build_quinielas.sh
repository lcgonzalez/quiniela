#!/bin/bash

# Usage: build_quinielas.sh [update]

# Script used to make the default structure of the quinielas, this is copy
# the fifa_official for each contestant, and if the option update is given
# to the script, update the football.db file.

# Read the file with the name of the contestants
contestantsPath=../../dataBases/info/contestants.txt
opwd=$(pwd)

while read line
do
	# Ignore commentaries
	[[ "$line" =~ ^#.*$ ]] && continue
	echo "Processing: $line"
	# Get contestant number
	IFS=':' read -a fields <<<"$line"
	i=${fields[0]}
	if [ "$i" == 0 ]; then
		continue
	fi
	# Format contestant number to 2 digits
	printf -v j "%02d" $i
	tmpPath=../../dataBases/quiniela$j
	
	if [ "$1" == "update" ]; then
		cd $tmpPath/openfootball/build/
		rake update DATA=worldcup2014
		cd $opwd
		./translateDB.sh ES $tmpPath/openfootball/build/build/football.db
	else # Build the directory structure, but not the database
		cp -r ../../dataBases/fifa_official $tmpPath
		echo $line > $tmpPath/owner.txt
		# Remove the directories that don't change and replace them with
		# soft links.
		cd $tmpPath
		rm -rf openmundi
		ln -s ../fifa_official/openmundi openmundi
		cd openfootball/
		rm -rf national-teams
		ln -s ../../fifa_official/openfootball/national-teams national-teams
		cd world-cup/2014--brazil/
		rm -f cup.txt cup_finals.txt
		ln -s ../../../../textDB/quiniela$j/cup.txt cup.txt
		ln -s ../../../../textDB/quiniela$j/cup_finals.txt cup_finals.txt
		cd $opwd	
	fi
done < $contestantsPath
