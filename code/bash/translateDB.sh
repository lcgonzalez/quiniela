#!/bin/bash

# Usage: translateDB.sh [Language] [Path_to_database]
#	Expample: ./translateDB.sh ES ../football.db
# Script used to translate the country names on the teams tables inside
# the football.db database.

dbPath=$2
translationPath=../../dataBases/info/countryNames_$1.txt
echo $translationPath

# Read the file wtih translation info.
while read line
do
	# Ignore the commentaries (lines that start with #)
	[[ "$line" =~ ^#.*$ ]] && continue
	# Get translation
	IFS=':' read -a fields <<<"$line"
	eng=${fields[0]}
	esp=${fields[1]}
	# Remove blank characters
	# Issue #1: It also removes the spaces between country names that has
	# more than two words (IE. United States).
	#eng=${eng//[[:blank:]]/}
	#esp=${esp//[[:blank:]]/}
	sqlite3 $dbPath "UPDATE teams SET title='$esp' WHERE title='$eng';"
done < $translationPath
