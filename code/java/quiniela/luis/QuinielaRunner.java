package luis;

public class QuinielaRunner
{
	public static void main(String[] args) {
		Quiniela official = new Quiniela();
		Quiniela quiniela1 = new Quiniela();
		if( args.length != 0  ) {
			official.build("../../../dataBases/fifa_official/openfootball/build/build/football.db");
			quiniela1.build(args[0]);
			quiniela1.compare(official);
			System.out.println("Score: " + quiniela1.getScore());
			
			Match[] matches;
			matches = quiniela1.getMatchesByRound(1);
			for( Match tmpMatch : matches ) {
				//System.out.println(tmpMatch);
			}
			QuinielaRank qr = new QuinielaRank();
			qr.setOfficial(official);
			qr.buildDB("../../../dataBases/info/contestants.txt", "../../../dataBases/", "test.db");
		}
	}
}
