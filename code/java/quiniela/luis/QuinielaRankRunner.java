package luis;

public class QuinielaRankRunner
{
	public static void main(String[] args) {
		if( args.length != 4  ) {
			System.out.println("Not enough parameters!");
			System.out.println("Usage: QuinielaRankRunner [FIFA_official_database_path] " +
				"[quiniela_contestants_source_textDB_path] " +
				"[quiniela_contestants_build_database_path] " +
				"[quiniela_rank_build_database_path]");
			System.out.println("Example:");
			System.out.println("QuinielaRankRunner ../../../dataBases/fifa_official/openfootball/build/build/football.db " +
				"../../../dataBases/info/contestants.txt " +
				"../../../dataBases/ rankings.db");
			return;
		}
		Quiniela official = new Quiniela();
		official.build(args[0]);
		QuinielaRank qr = new QuinielaRank();
		qr.setOfficial(official);
		qr.buildDB(args[1], args[2], args[3]);
	}
}
