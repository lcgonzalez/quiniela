package luis;

import java.sql.*;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Quiniela 
{
	Match[] matches;
	private int score;
	private Map<Integer, Integer> rounds; //Round 16, quarter finals, semi-finals, ...
	private Map<Integer, Integer> matchesPerRound;
	public Quiniela() {
		score = 0;
		initRounds();
		initMatchesPerRound();
	}
	private void initMatchesPerRound() {
		matchesPerRound = new HashMap<Integer, Integer>();
		matchesPerRound.put(1, 48); //Eliminatorias
		matchesPerRound.put(2, 8); //Octavos de final
		matchesPerRound.put(4, 4); //Cuartos de final
		matchesPerRound.put(8, 2); //Semifinal
		matchesPerRound.put(16, 2); //Tercer lugar y final
	}
	private void initRounds() {
		rounds = new HashMap<Integer, Integer>();
		rounds.put(49, 2);
		rounds.put(50, 2);
		rounds.put(51, 2);
		rounds.put(52, 2);
		rounds.put(53, 2);
		rounds.put(54, 2);
		rounds.put(55, 2);
		rounds.put(56, 2);
		rounds.put(57, 4);
		rounds.put(58, 4);
		rounds.put(59, 4);
		rounds.put(60, 4);
		rounds.put(61, 8);
		rounds.put(62, 8);
		rounds.put(63, 16);
		rounds.put(64, 16);
	}
	public void build(String dbPath) {
		//System.out.println("Trying to open " + dbPath);
		Connection c = null;
		Statement gamesStmt, teamsStmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
			gamesStmt = c.createStatement();
			teamsStmt = c.createStatement();
			ResultSet gamesSet = gamesStmt.executeQuery(
					"SELECT COUNT(*) FROM games " +
    					"WHERE play_at >='2014-06-12 00:00:00.00000' " +
    					"ORDER BY group_id; ");
			int numberOfMatches = gamesSet.getInt(1);
			matches = new Match[numberOfMatches];
			gamesSet = gamesStmt.executeQuery(
					"SELECT * FROM games " +
    					"WHERE play_at >='2014-06-12 00:00:00.00000' " +
    					"ORDER BY group_id; ");
			ResultSet teamsSet = null;
			int i = 0;
			while(gamesSet.next()) {
				int team1_id = gamesSet.getInt("team1_id");
				int team2_id = gamesSet.getInt("team2_id");
				int score1 = gamesSet.getInt("score1");
				int score2 = gamesSet.getInt("score2");
				int pos = gamesSet.getInt("pos");
				Date play_at = gamesSet.getDate("play_at");
				teamsSet = teamsStmt.executeQuery(
						"SELECT * FROM teams " +
    		   				"WHERE id=" + team1_id + ";");
				String team1 = teamsSet.getString("title");
				teamsSet = teamsStmt.executeQuery(
					"SELECT * FROM teams " + 
    		   			"WHERE id=" + team2_id + ";");
				String team2 = teamsSet.getString("title");
				matches[i] = new Match();
				Match tmpMatch = matches[i++];
				tmpMatch.setTeam1Name(team1);
				tmpMatch.setTeam2Name(team2);
				tmpMatch.setTeam1Score(score1);
				tmpMatch.setTeam2Score(score2);
				tmpMatch.setDate(play_at);
				tmpMatch.setRound(getMatchRound(pos));
				//System.out.println(tmpMatch.getDate());
				//System.out.println(tmpMatch);
			}
			gamesSet.close();
			if( teamsSet != null){
				teamsSet.close();
			}
			gamesStmt.close();
			teamsStmt.close();
			c.close();
		} catch( Exception e) {
			System.out.println( e.getClass().getName() +
				": " + e.getMessage() );
		}
	}
	
	public void compare(Quiniela tmpQuiniela) {
		try {
			Match[] tmpMatches = tmpQuiniela.getMatches();
			int i=0;
			Date todayDate = new Date();
			for( Match tmpMatch : matches) {
				if (todayDate.after(tmpMatch.getDate()) ) {
					score += tmpMatch.compare(tmpMatches[i++]);
				}
				//System.out.println(tmpMatch);
			}
		} catch (Exception e ) {
			System.out.println(e.getMessage());
		}
	}
	
	private int getMatchRound(int round_id) {
		if( round_id < 49 ) {
			return 1;
		}
		return (Integer)rounds.get(round_id);
	}

	public Match[] getMatches() {
		return matches;
	}
	private int getMatchesPerRound(int round) {
		return (Integer)matchesPerRound.get(round);
	}

	public Match[] getMatchesByRound(int round) {
		
		Match[] tmpMatches = new Match[getMatchesPerRound(round)];
		int i=0;
		int j=0;
		for(Match tmpMatch : matches) {
			if( tmpMatch.getRound() == round ) {
				tmpMatches[i++] = matches[j];
				//System.out.println(matches[j]);
			}
			j++;
		}	
		return tmpMatches;
	}
	
	public int getScore() {
		return score;
	}
}
