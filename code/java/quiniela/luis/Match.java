package luis;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Match
{
	private String team1Name;
	private String team2Name;
	private int team1Score;
	private int team2Score;
	private SimpleDateFormat dateFormat;
	private int round;
	private Date date;
	private int score; //2, 1 or 0 points
	public Match () {
		dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		score = -1; //Aún no se juega el partido
	}
	public int compare(Match tmpMatch) {
		if( (team1Score == tmpMatch.getTeam1Score()) &&
			(team2Score == tmpMatch.getTeam2Score()) ) {
			//Le atinó al marcador
			score = 2*round;
			return score;
		} else if( ((team1Score < team2Score) && (tmpMatch.getTeam1Score() < tmpMatch.getTeam2Score())) || //El equipo 2 gana
			((team1Score == team2Score) && (tmpMatch.getTeam1Score() == tmpMatch.getTeam2Score())) || //Los equipos empatan
			((team1Score > team2Score) && (tmpMatch.getTeam1Score() > tmpMatch.getTeam2Score())) ) { //El equipo 1 empata
			//No le atinó al marcador pero sí al ganador o al empate
			score = 1*round;
			return score;
		} else {
			//No le atinó a nada
			score = 0;
			return score;
		}	
	}
	@Override
	public String toString() {
		String tmpStr;
		if( score == -1 ) {
			tmpStr = "?";
		} else {
			tmpStr = Integer.toString(score);
		}	
		return tmpStr + " : " + team1Name + " [" + team1Score + "] vs [" +
			team2Score + "] " + team2Name;
	}
	public String getTeam1Name() {
		return team1Name;
	}
	public void setTeam1Name(String name) {
		team1Name = name;
	}
	public String getTeam2Name() {
		return team2Name;
	}
	public void setTeam2Name(String name) {
		team2Name = name;
	}
	public int getTeam1Score() {
		return team1Score;
	}
	public void setTeam1Score(int score) {
		team1Score = score;
	}
	public int getTeam2Score() {
		return team2Score;
	}
	public void setTeam2Score(int score) {
		team2Score = score;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date newDate) {
		date = newDate;
	}
	public int getRound() {
		return round;
	}
	public void setRound(int newRound) {
		round = newRound;
	}
}
