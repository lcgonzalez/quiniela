package luis;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Map;
import java.util.TreeMap;
import java.sql.*;

public class QuinielaRank {
	
	Quiniela official;
	Map<Integer, Integer> ranking; // Map<quinielaID, score>
	public QuinielaRank() {
		ranking = new TreeMap<Integer,Integer>();
	}

	public void setOfficial(Quiniela newOfficial) {
		official = newOfficial;
	}

	private void buildSqliteDB(String dbPath) {
		Connection c = null;
		Statement rankStmt = null;
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + dbPath);
			rankStmt = c.createStatement();
			//String sql = "SELECT count(*) FROM sqlite_master WHERE type = 'table' AND name = 'quinielas';";
			String sql = "CREATE TABLE IF NOT EXISTS quinielas" +
				"(id INT PRYMARY KEY NOT NULL, " +
				"score INT NOT NULL);";
			rankStmt.executeUpdate(sql);
			
			for(Map.Entry<Integer, Integer> entry : ranking.entrySet()) {
				sql = "SELECT EXISTS(SELECT 1 FROM quinielas " +
					"WHERE id='" + entry.getKey() + "' LIMIT 1);";
				if( rankStmt.executeQuery(sql).getInt(1) == 0) { //check if record doesn't exist
					sql = "INSERT INTO quinielas (id,score) " +
						"VALUES (" + entry.getKey() + ", " +
						entry.getValue() + ")"; 
				} else { // the record exists, update it!
					sql = "UPDATE quinielas SET score=" +
						entry.getValue() + " WHERE id=" +
						entry.getKey() + ";";
				}
				rankStmt.executeUpdate(sql);
			}
			rankStmt.close();
			//c.commit(); //uncomment if database is NOT in auto-commit mode
			c.close();

		} catch ( Exception e) {
			System.out.println(e.getClass().getName() + ": " +
				e.getMessage() );
		}
	}

	public void buildDB(String contestantsPath, String quinielasPath, String rankDbPath) {
		BufferedReader br = null;
		try {
			String sLine;
			br = new BufferedReader(new FileReader(contestantsPath));
			while ((sLine = br.readLine()) != null) {
				// Ignore commentaries
				if(sLine.trim().startsWith("#")) continue;
				// split quiniela number and contestant
				String[] fields = sLine.split(":");
				int i= Integer.parseInt(fields[0].trim());
				String j = String.format("%02d", i);
				String quinielaDbPath = new String(quinielasPath + "quiniela" +
					j + "/openfootball/build/build/football.db");
				Quiniela tmpQ = new Quiniela();
				tmpQ.build(quinielaDbPath);
				tmpQ.compare(official);
				ranking.put(i, tmpQ.getScore());
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			try {
				if (br != null) br.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
		buildSqliteDB(rankDbPath);
	} 
}
