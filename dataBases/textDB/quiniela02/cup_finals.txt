######################
# Finals

(16) Round of 16

(49) Sat Jun/28   Brazil 2-1 Netherlands     @ Belo Horizonte
(50) Sat Jun/28   Japan 1-2 Italy     @ Rio de Janeiro

(51) Sun Jun/29   Spain 1-2 Mexico     @ Fortaleza
(52) Sun Jun/29   England 2-0 Colombia     @ Recife

(53) Mon Jun/30   Ecuador 1-1 Nigeria     @ Brasília
(54) Mon Jun/30   Germany 2-1 Belgium     @ Porto Alegre

(55) Tue Jul/1    Argentina 2-1 France     @ São Paulo
(56) Tue Jul/1    South Korea 1-2 Portugal     @ Salvador


(17) Quarter-finals

(57) Fri Jul/4      Brazil 2-1 Italy    @ Fortaleza
(58) Fri Jul/4      Ecuador 1-2 Germany    @ Rio de Janeiro

(59) Sat Jul/5      Mexico 1-0 England    @ Salvador
(60) Sat Jul/5      Argentina 3-2 Portugal    @ Brasília


(18) Semi-finals

(61) Tue Jul/8      Brazil 3-2 Germany      @ Belo Horizonte
(62) Wed Jul/9      Mexico 1-1 Argentina      @ São Paulo


(19) Match for third place

(63) Sat Jul/12     Germany 1-0 Mexico   @ Brasília


(20) Final

(64) Sun Jul/13     Brazil 3-2 Argentina   @ Rio de Janeiro
