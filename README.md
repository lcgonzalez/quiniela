# LEEME #

### ¿Para qué es este repositorio? ###

* **Resumen rápido**:
Applicación para generar y usar una base de datos de quinielas automaticamente, basados en archivos de texto con los juegos del mundial.
* **Version**:
1.0
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### Como echar a andar el proyecto ###

* **Resumen**:
Se asume que el proyecto se ejecutara en un entorno *unix.
Todas las rutas aquí presentadas son relativas a la raíz del proyecto.

* **Configuración general**:
Para comenzar hay que descargar y construir la base de datos oficial de la FIFA, ejecutando el siguiente script:
```
#!bash

    $: ./code/bash/build_official.sh
```
Despues hay que llenar los archivos de configuración de las quinielas, ubicados en las siguientes rutas:
1.  **./code/dataBases/info/contestants.txt** Archivo con los participantes de la quiniela, así como el número de quiniela asociado.
1.  **./code/dataBases/info/quiniela?** Ver la siguiente sección **Configuración de las quinielas** para instruccciones de como editar este archivo.

* **Configuración de las quinielas**:
Para modificar la base de datos de quinielas (en modo texto), favor de dirigirse al directorio **/dataBase/info/quiniela?** en donde el '**?**' representa el número de quiniela que se desea modificar. Dentro de este directorio existen dos archivos:

1.  **cup.txt** Este archivo contiene la información de la primer fase del mundial.
2.  **cup_finals.txt** Este archivo contiene la información desde los octavos de final.
Para modificar los partidos hay que cambiar el nombre de los equipos (tiene que estar en inglés) así como el marcador. No hay que olvidar descomentar la linea (borrar el simbolo '#') si es que la linea está comentada.
Ejemplo:

```
#!bash

    (1) Thu Jun/12 17:00   Brazil 3-0 Croatia       @ Arena de São Paulo, São Paulo (UTC-3)
                             # Neymar 29', 71' (pen.) Oscar 90+1';  Marcelo 11' (o.g.)

    (2) Fri Jun/13 13:00   Mexico 2-1 Cameroon      @ Estádio das Dunas, Natal (UTC-3)
                             # Peralta 61'
    # El siguiente partido está comentado por lo tanto no cuenta.
    ##(17) Tue Jun/17 16:00   Brazil 2-1 Mexico        @ Estádio Castelão, Fortaleza (UTC-3)
    (18) Wed Jun/18 18:00   Cameroon 1-1 Croatia     @ Arena Amazônia, Manaus (UTC-4)

```

* **Dependencias**:
Se asume que el projecto se ejecutara en un entorno *unix con las siguientes herramientas:

1. **rake** (ruby make)
1. **bash** 
1. **javac**